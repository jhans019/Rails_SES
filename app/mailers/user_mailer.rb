require 'nokogiri'
require 'html-proofer' 
require 'find'

class UserMailer < ApplicationMailer
  after_action :deliver_email

  def send_test_email(params)
    @subject = params[:subject]
    @message = convert_special_characters_in(params[:message])
    File.open("file.html", 'w') { |file| file.write(@message) }
    HTMLProofer.check_file('file.html').run
  end

  private

    def deliver_email
      mail(:to => ENV['TO_EMAIL'], :subject => @subject)
    end

    def convert_special_characters_in(message)
      fixed_html = Nokogiri::HTML::DocumentFragment.parse(message).to_html
      return fixed_html.html_safe
    end

end
